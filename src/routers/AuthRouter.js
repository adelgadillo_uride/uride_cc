import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import SignIn from '../components/signIn/SignIn'

export const AuthRouter = () => {
    return (
        <div className='auth__main'>
            <div className='auth__box-container'>
                <Switch>
                    <Route exact path="/auth/login" component={SignIn}/>

                    <Redirect to="/auth/login"/>
                </Switch>
            </div>
            {/* Switch */}
            {/* path auth/login component loginpage */}
            {/* path auth/register component register */}
            {/* redirect auth/login */}
            
        </div>
    )
}
