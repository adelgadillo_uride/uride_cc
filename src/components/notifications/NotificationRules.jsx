import React, { useEffect } from 'react';

import { Box, Button, ButtonGroup, Card, CardActions, CardContent, Chip, Grid, IconButton, makeStyles, Select, Tab, Tabs, Typography, Input, MenuItem, InputLabel, FormControl, ThemeProvider, withStyles, createTheme } from '@material-ui/core';
import { green } from '@material-ui/core/colors';
import { Add, Pause } from '@material-ui/icons';
import { useState } from 'react';


const GreenButton = withStyles((theme) => ({
    root: {
        color: theme.palette.getContrastText(green[700]),
        backgroundColor: green[500],
        '&:hover': {
            backgroundColor: green[700],
        },
    },
}))(Button);

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

export default function NotificationRules(props) {

    const [rules, setRules] = useState(props.rules);
    const [scopes, setScopes] = useState(props.scopes);
    const [oldRules, setOldRules] = useState([]);
    const [msgs, setMsgs] = useState(props.msgs);
    const [editing, setEditing] = useState(false);

    useEffect(() => {
        updateOldRules();
    }, []);

    const updateOldRules = () => {
        var oldArr = [];
        rules.map(rule => {
            oldArr.push({ ...rule });
        });
        setOldRules(oldArr);
    };

    const createRule = () => {
        setEditing(true);
        const newRule = {
            id: "",
            title: "",
            status: "",
            scope: [],
            msg: "",
            description: "",
            running: false,
            edit: true
        };

        let rls = [...rules, newRule];
        setRules(rls);
    };

    const saveRule = (rule, index) => {
        let rls = [...rules];
        if (rule.id === "")
            rule.id = "" + (Math.random() * 10000).toFixed(0);
        rule.edit = false;
        rls[index] = rule;
        setRules(rls);
        updateOldRules();
        setEditing(false);
        props.onUpdateRules(rls);
    };

    const enableEdit = (index) => {
        setEditing(true);
        let tmpRules = [...rules];
        let rule = tmpRules[index];
        rule.edit = true;
        setRules(rules);
    };

    const disableEdit = () => {
        let tempRules = [...rules];
        tempRules.map((rule) => rule.edit = false);
        setEditing(false);
        setRules(oldRules);
    };

    const handleSelect = (event, index) => {
        let tempRules = [...rules];
        tempRules[index].msg = event.target.value;
        setRules(tempRules);
    };

    const handleScopeChange = (event, index) => {
        let tempRules = [...rules];
        tempRules[index].scope = event.target.value;
        setRules(tempRules);
    };

    const handleTitleChange = (event, index) => {
        let tempRules = [...rules];
        tempRules[index].title = event.target.value;
        setRules(tempRules);
    };

    const cancelRule = (rule, index) => {
        let tempRules = [...rules];
        if (rule.id === "") {
            tempRules = tempRules.filter(rule => rule.id !== "");
            setRules(oldRules);
            setEditing(false);
        } else {
            disableEdit();
        }
    };

    return (
        <>
            <div className="header">
                <h3>Notification rules</h3>
                <div className="optionButtons">
                    <Button
                        variant="contained"
                        color="secondary"
                        className={props.classes.button}
                        startIcon={<Pause />}
                    >
                        Pause all
                    </Button>

                    <Button
                        onClick={createRule}
                        variant="contained"
                        color="primary"
                        className={props.classes.button}
                        startIcon={<Add />}
                    >
                        Create new
                    </Button>
                </div>

            </div>
            <br />
            <Grid container spacing={1}>
                {
                    rules.map((rule, index) => (
                        <Grid key={"rule_" + index} item xs={12} sm={6} lg={3}>
                            <Card>
                                <CardContent>
                                    {
                                        rule.edit &&
                                        <>
                                            <FormControl disabled={!rule.edit}>
                                                <InputLabel htmlFor="title">Title</InputLabel>
                                                <Input id="title" value={rule.title} onChange={(event) => handleTitleChange(event, index)} />
                                            </FormControl>
                                            <br />
                                            <br />
                                        </>
                                    }
                                    {
                                        !rule.edit &&
                                        <div className="cardTitle">
                                            {rule.title}
                                        </div>
                                    }

                                    <Chip
                                        variant="default"
                                        color="primary"
                                        size="small"
                                        label="Active" />
                                    <br />
                                    <Select disabled={!rule.edit}
                                        className={props.classes.dropdown}
                                        labelId="demo-mutiple-name-label"
                                        id="demo-mutiple-name"
                                        value={rule.msg}
                                        onChange={(event) => { handleSelect(event, index) }}
                                        input={<Input />}
                                    >
                                        {msgs.map((msg) => (
                                            <MenuItem key={msg.id} value={msg.id}>
                                                {msg.title}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                    <br />
                                    <FormControl className={props.classes.formControl} disabled={!rule.edit}>
                                        <InputLabel id="demo-mutiple-chip-label">Scope</InputLabel>
                                        <Select
                                            labelId="demo-mutiple-chip-label"
                                            id="demo-mutiple-chip"
                                            multiple
                                            value={rule.scope}
                                            onChange={(event) => handleScopeChange(event, index)}
                                            input={<Input id="select-multiple-chip" />}
                                            renderValue={(selected) => (
                                                <div className={props.classes.chips}>
                                                    {selected.map((value) => (
                                                        <Chip key={value} label={value} className={props.classes.chip} />
                                                    ))}
                                                </div>
                                            )}
                                            MenuProps={MenuProps}
                                        >
                                            {scopes.map((scope) => (
                                                <MenuItem key={scope} value={scope}>
                                                    {scope}
                                                </MenuItem>
                                            ))}
                                        </Select>
                                    </FormControl>

                                </CardContent>
                                <CardActions className={props.classes.cardActions}>
                                    {
                                        !rule.edit &&
                                        <>
                                            <Button size="medium" color="primary" onClick={() => enableEdit(index)} >
                                                Edit
                                            </Button>
                                            {
                                                !rule.running &&
                                                // <ThemeProvider  theme={theme}>
                                                <GreenButton variant="contained" className={props.classes.margin}>
                                                    Activate
                                                </GreenButton>
                                                // </ThemeProvider>
                                            }
                                            {
                                                rule.running &&
                                                <Button size="medium" variant="contained" color="secondary">
                                                    Stop
                                                </Button>
                                            }
                                        </>
                                    }

                                    {
                                        rule.edit &&
                                        <>
                                            <Button size="medium" onClick={() => saveRule(rule, index)} color="primary">
                                                Save
                                            </Button>
                                            <Button size="medium" onClick={() => cancelRule(rule, index)} color="secondary">
                                                Cancel
                                            </Button>
                                        </>
                                    }
                                </CardActions>
                            </Card>
                        </Grid>
                    ))
                }
            </Grid>
        </>
    )
}
