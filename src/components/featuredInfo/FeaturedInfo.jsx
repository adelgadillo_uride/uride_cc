import React from 'react';
import "./featuredInfo.css";

import { ArrowDownward, ArrowUpward } from "@material-ui/icons";
import { Box, Grid, makeStyles, Paper } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        border: 'none'
    },
}));

export default function FeaturedInfo() {

    const classes = useStyles();

    return (
        <>
            <div className={classes.root}>
                <Box>
                    <Grid className="actionGrid" container spacing={1}>
                        <Grid item xs={1}>
                            <Paper>
                                ola k ase
                            </Paper>
                        </Grid>
                    </Grid>
                </Box>
            </div>
        </>
    );
}