import React from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DashboardIcon from '@material-ui/icons/Dashboard';
import LocalTaxiIcon from '@material-ui/icons/LocalTaxi';
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople';
import AvTimerIcon from '@material-ui/icons/AvTimer';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import ReceiptIcon from '@material-ui/icons/Receipt';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import SettingsIcon from '@material-ui/icons/Settings';
import MapOutlinedIcon from '@material-ui/icons/MapOutlined';
import NotificationsIcon from '@material-ui/icons/Notifications';
import { Accordion, AccordionActions, AccordionDetails, AccordionSummary } from '@material-ui/core';
import { CalendarTodayOutlined, Explore, ExpandMore } from "@material-ui/icons";
// import CenteredGrid from '../centeredGrid/CenteredGrid';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";
import Reliability from '../../pages/reliability/Reliability';
import { Drivers } from '../../pages/drivers/Drivers';
import { Notifications } from '../../pages/notifications/Notifications';
import Dashboard from '../../pages/dashboard/Dashboard';
import Overview from '../../pages/home/Overview';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { logout, startLogout } from '../../actions/auth';
// import { useReducer } from 'react';
// import { authReducer } from '../../reducers/authReducer';
import { useEffect } from 'react';
const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    overflow: 'auto'
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  heading: {
    width: '123px',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
  },
  content: {
    flexGrow: 4,
    padding: theme.spacing(3),
    overflow: 'auto' // to adjust contents of main frame to total available space
  },
}));

const init = () => {
  return JSON.parse(localStorage.getItem('user')) || {loggedIn:false};
}

export default function MiniDrawer() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const [expanded, setExpanded] = React.useState(false);

  const dispatch = useDispatch();

  // const [user] = useReducer(authReducer, {}, init);
  
  const [user, setUser] = useState({});
  
  useEffect(() => {
    setUser(init());
  }, [])

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setExpanded(false);
    setOpen(false);
  };

  const [appbarTitle, setAppbarTitle] = useState("");

  const setTitle = (title) => {
    setAppbarTitle(title);
  }

  const handleLogout = () => {
    dispatch(startLogout());
  }


  const menuOpts = [
    {
      name: 'Dashboard',
      icon: <DashboardIcon/>,
      component: Dashboard,
      renderedComponent: (props)=><Dashboard {...props} setAppbarTitle={setTitle}/>
    },
    {
      name: 'Overview',
      icon: <MapOutlinedIcon/>,
      component: Overview,
      renderedComponent: (props)=><Overview {...props} setAppbarTitle={setTitle}/>
    },
    {
      name: 'Schedule',
      icon: <CalendarTodayOutlined/>,
      component: Dashboard,
      renderedComponent: (props)=><Dashboard {...props} setAppbarTitle={setTitle}/>
    },
    {
      name: 'Notifications',
      icon: <NotificationsIcon/>,
      component: Notifications,
      renderedComponent: (props)=><Notifications {...props} setAppbarTitle={setTitle}/>
    },
    {
      name: 'Drivers',
      icon: <LocalTaxiIcon/>,
      component: Drivers,
      renderedComponent: (props)=><Drivers {...props} setAppbarTitle={setTitle}/>
    },
    {
      name: 'Passengers',
      icon: <EmojiPeopleIcon/>,
      component: Dashboard,
      renderedComponent: (props)=><Dashboard {...props} setAppbarTitle={setTitle}/>
    },
    {
      name: 'Rides',
      icon: <Explore/>,
      component: Dashboard,
      renderedComponent: (props)=><Dashboard {...props} setAppbarTitle={setTitle}/>
    },
    {
      name: 'Reliability',
      icon: <AvTimerIcon/>,
      component: Reliability,
      renderedComponent: (props)=><Reliability {...props} setAppbarTitle={setTitle}/>
    },
    {
      name: 'Earnings',
      icon: <AccountBalanceIcon/>,
      component: Dashboard,
      renderedComponent: (props)=><Dashboard {...props} setAppbarTitle={setTitle}/>
    },
    {
      name: 'Bookings',
      icon: <ReceiptIcon/>,
      component: Dashboard,
      renderedComponent: (props)=><Dashboard {...props} setAppbarTitle={setTitle}/>
    },
    {
      name: 'Access',
      icon: <LockOutlinedIcon/>,
      component: Dashboard,
      renderedComponent: (props)=><Dashboard {...props} setAppbarTitle={setTitle}/>
    },
    {
      name: 'Settings',
      icon: <SettingsIcon/>,
      component: Dashboard,
      renderedComponent: (props)=><Dashboard {...props} setAppbarTitle={setTitle}/>
    },
      
  ]

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        color='inherit'
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={!open ? handleDrawerOpen : handleDrawerClose}
            edge="start"
            className={clsx(classes.menuButton, {
              //   [classes.hide]: open,
            })}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap>
            {appbarTitle}
          </Typography>
        </Toolbar>
      </AppBar>
      {/* <Router> */}
        <Drawer
          variant="permanent"
          className={clsx(classes.drawer, {
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          })}
          classes={{
            paper: clsx({
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open,
            }),
          }}
        >
          <div>
            <Accordion square expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
              <AccordionSummary className="userOptionsHeader"
                expandIcon={<ExpandMore />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <img src={user.photoURL || "https://images.pexels.com/photos/8050897/pexels-photo-8050897.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"} alt="" className="topAvatar" />
                <div >
                  <Typography noWrap={false} className={classes.heading}>{user.displayName}</Typography>
                  <Typography variant='caption'>Operations Manager</Typography>
                </div>
              </AccordionSummary>
              <AccordionActions>
                <List>
                  <ListItem button key="logoutButton" onClick={handleLogout} component={Link} to="/">
                    <ListItemText primary="Logout"/>
                  </ListItem>
                </List>
              </AccordionActions>
            </Accordion>
          </div>
          <Divider />

          <List>
            {menuOpts.map((element, index) => (
              <ListItem button selected={element.name === appbarTitle} key={element.name} component={Link} to={"/" + element.name.toLowerCase()}>
                <ListItemIcon>{element.icon}</ListItemIcon>
                <ListItemText primary={element.name} />
              </ListItem>
            ))}
          </List>
        </Drawer>
          <Switch>
            {
              menuOpts.map((element, index)=>(
                <Route key={index} path={"/"+element.name} exact name={element.name} render={(props)=>element.renderedComponent({...props, title:element.name})} />
                ))
            }
            <Route path="/" render={(props)=><Dashboard {...props} title={"Dashboard"} setAppbarTitle={setTitle} />} />
            {/* <Redirect to="/"/> */}
          </Switch>
    </div>
  );
}
