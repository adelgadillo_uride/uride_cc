import React from 'react'

export default function Main(props) {
    // console.log(props);
    return (
        <main className={props.className}>
            {props.children}
        </main>
    )
}
