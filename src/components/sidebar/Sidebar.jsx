import React from 'react';
import './sidebar.css';

import { LineStyle, MapOutlined, ExploreOutlined, EmojiPeopleOutlined, LocalTaxiOutlined, CalendarTodayOutlined } from "@material-ui/icons";

export default function Sidebar () {
    return ( 
        <div className="sidebar">
            <div className="sidebarWrapper">
                <div className="sidebarMenu">
                    {/* <Typography variant="">Management</Typography> */}
                    <h3 className="sidebarTitle">MANAGEMENT</h3>
                    <ul className="sidebarList">
                        <li className="sidebarListItem active">
                            <LineStyle className="sidebarIcon"  />
                            Home
                        </li>
                    </ul>
                </div>
                <div className="sidebarMenu">
                    <h3 className="sidebarTitle">OPERATIONS</h3>
                    <ul className="sidebarList">
                        <li className="sidebarListItem">
                            <MapOutlined className="sidebarIcon"  />
                            Overview
                        </li>
                        <li className="sidebarListItem">
                            <CalendarTodayOutlined className="sidebarIcon" />
                            Schedule
                        </li>
                        <li className="sidebarListItem">
                            <LocalTaxiOutlined className="sidebarIcon"  />
                            Drivers
                        </li>
                        <li className="sidebarListItem">
                            <EmojiPeopleOutlined className="sidebarIcon"  />
                            Passengers
                        </li>
                        <li className="sidebarListItem">
                            <ExploreOutlined className="sidebarIcon"  />
                            Rides
                        </li>
                    </ul>
                </div>
                <div className="sidebarMenu">
                    <h3 className="sidebarTitle">REPORTS</h3>
                    <ul className="sidebarList">
                        <li className="sidebarListItem">
                            <MapOutlined className="sidebarIcon"  />
                            Overview
                        </li>
                        <li className="sidebarListItem">
                            <CalendarTodayOutlined className="sidebarIcon" />
                            Schedule
                        </li>
                        <li className="sidebarListItem">
                            <LocalTaxiOutlined className="sidebarIcon"  />
                            Drivers
                        </li>
                    </ul>
                </div>
                <div className="sidebarMenu">
                    <h3 className="sidebarTitle">SETTINGS</h3>
                    <ul className="sidebarList">
                        <li className="sidebarListItem">
                            <MapOutlined className="sidebarIcon"  />
                            Overview
                        </li>
                        <li className="sidebarListItem">
                            <CalendarTodayOutlined className="sidebarIcon" />
                            Schedule
                        </li>
                    </ul>
                </div>
            </div>
        </div>
     );
}