export const actionTypes = {
    login:  '[Auth] login',
    logout: '[Auth] logout',
    auth: '[Auth] auth',
    saveCredential: '[Auth] saveCredential', 
    checkUser: '[Auth] checkUser',
// 
    uiSetError: '[ui] Set Error',
    uiRemoveError: '[ui] Remove Error',

    uiStartLoading: '[UI] Start loading',
    uiFinishLoading: '[UI] Finish loading',
}