import { AppRouter } from "./routers/AppRouter";
import { Provider } from 'react-redux'
import { store } from './store/store'

function CommandCenterApp() {
  return (
    <div className="App">
      <div className="container">
        <Provider store={store}>
          <AppRouter />
        </Provider>
      </div>
    </div>
  );
}

export default CommandCenterApp;
