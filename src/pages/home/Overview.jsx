import { Card, CardContent, CardHeader, Grid, Icon, IconButton, makeStyles, Paper, Typography } from '@material-ui/core';
import { Close, ExpandMore, MoreVert, OpenInNew } from '@material-ui/icons';
import GoogleMapReact from 'google-map-react';
import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import Main from '../../components/main/Main';
import OperationFeed from '../../components/operationFeed/OperationFeed';
import { Tooltip } from 'recharts';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        // padding: '20px',
        // top: '100px',
        // position: 'absolute',
        // zIndex:1
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        border: 'none',
        height: '100%'
    },
    floatingGrid: {
        position: 'absolute',
        zIndex: 1,
        // width: 'calc(100% - 162px)',
        padding: '10px',
    },
    extended: {
        height:'100%'
    },
    hidden: {
        display: 'none'
    }
}));

const statusItems = [
    {
        title: 'Booked rides',
        value: 320
    },
    {
        title: 'Cancelled rides',
        value: 28
    },
    {
        title: 'On queue',
        value: 66
    },
    {
        title: 'Available drivers',
        value: 78
    },
    {
        title: 'Active drivers',
        value: 15
    },
]

const eventArr = [
    {
        driver: "Cody Fisher",
        lat: 48.382500,
        lng: -89.253897,
        status: "En Route",
        action: "Doing something"
    },
    {
        driver: "Eleanor Peña",
        lat: 48.378471,
        lng: -89.261989,
        status: "On queue",
        action: "Doing something"
    },
    {
        driver: "Bill Jones",
        lat: 48.365091,
        lng: -89.281352,
        status: "Dropped off",
        action: "Doing something"
    },
];

const getInfoWindowString = (place) => `
    <div>
      <div style="font-size: 16px;">
        ${place.driver}
      </div>

      <div style="font-size: 14px; color: green;">
        ${place.status}
      </div>
      <div style="font-size: 14px; color: grey;">
        ${place.action}
      </div>
    
    </div>`;

const AnyReactComponent = ({ text, open, handleTooltipClose}) => 
    <div>
        {/* <Tooltip
            onClose={handleTooltipClose}
            open={open}
            title={
                <React.Fragment>
                  <Typography color="inherit">Tooltip with HTML</Typography>
                  <em>{"And here's"}</em> <b>{'some'}</b> <u>{'amazing content'}</u>.{' '}
                  {"It's very engaging. Right?"}
                </React.Fragment>
              }
        > */}
            <Icon>
                <img id="markerIcon" src="assets/map_icon_std.svg"/>
            </Icon>
        {/* </Tooltip> */}
    </div>;

const handleApiLoaded = (map, maps) => {
    const markers = [];
    const infowindows = [];
  
    eventArr.forEach((place) => {
      markers.push(new maps.Marker({
        position: {
          lat: place.lat,
          lng: place.lng,
        },
        map,
      }));
  
      infowindows.push(new maps.InfoWindow({
        content: getInfoWindowString(place),
      }));
    });
  
    markers.forEach((marker, i) => {
      marker.addListener('mouseover', () => {
        infowindows[i].open(map, marker);
      });

      marker.addListener('mouseout', () => {
        infowindows[i].close();
      })
    });
  };
  
export default function Overview(props) {

    const [selected, setSelected] = useState(-1);
    const [showTooltip, setShowTooltip] = useState(false);

    const handleTooltipOpen = () => {
        setShowTooltip(true);
    };
    
    const handleTooltipClose = () => {
        setShowTooltip(false);
    };

    const classes = useStyles();

    useEffect(() => {
        props.setAppbarTitle(props.title);
    }, [])


    const defaultProps = {
        google: "https://maps.googleapis.com/maps/api/js?key=&v=3.exp&libraries=places",
        center: {
            lat: 48.382221,
            lng: -89.246109
        },
        zoom: 11
    };

    const openWidget = (index) => {
        console.log(index);
        setSelected(index);
    }

    const ModelsMap = (map, maps) => {
        //instantiate array that will hold your Json Data
        let dataArray = [];
        //push your Json Data in the array
        {
            eventArr.map(markerJson => dataArray.push(markerJson));
        }
        debugger;
        //Loop through the dataArray to create a marker per data using the coordinates in the json
        for (let i = 0; i < eventArr.length; i++) {
            let marker = new maps.Marker({
            position: { lat: eventArr[i].lat, lng: eventArr[i].lng },
            map,
            label: eventArr[i].driver
            });
        }
    };

    return (
        <Main className='overview'>
            <div>
                <div className={classes.root}>
                    <div className="stickyGrid">
                        <Grid container className={classes.floatingGrid} spacing={1}>
                            {
                                statusItems.map((element, index) =>
                                    <Grid item className={clsx({
                                        [classes.hidden]: index === selected,
                                    })} xs={3} sm={2}>
                                        <Paper className={classes.extended}>
                                            <div className="panelActionItem">
                                                <div className="panelActionItemHeader">
                                                    <div className="panelActionItemHeaderContent"></div>
                                                    <div className="panelActionItemHeaderAction">
                                                        <IconButton onClick={()=>openWidget(index)}>
                                                            <OpenInNew />
                                                        </IconButton>
                                                    </div>
                                                </div>
                                                <div className="panelActionItemTitle">
                                                    <Typography variant="overline">{element.title}</Typography>
                                                </div>
                                                <div className="panelActionItemValue">
                                                    <Typography variant="h5">{element.value}</Typography>
                                                </div>
                                            </div>
                                        </Paper>
                                    </Grid>
                                )
                            }
                            {/* <Grid item xs={2}>
                            </Grid>
                            <Grid item xs={8} sm={4}>
                                <Paper className={classes.extended}>
                                    <OperationFeed/>
                                </Paper>
                            </Grid> */}
                        </Grid>
                    </div>
                    <div className="feed">
                        <Paper className={classes.extended}>
                            <OperationFeed/>
                        </Paper>
                    </div>

                    <div className="liveData" style={selected === -1 ? {display:'none'}:{}}>
                        <Paper>
                            {/* widget */}
                            <OperationFeed title={selected !== -1 ? statusItems[selected].title:null} icon={Close} eventFunction={()=>openWidget(-1)}/>
                        </Paper>
                    </div>
                    
                    <div className="home">

                        <div style={{ height: '91vh' }}>

                            <GoogleMapReact
                                bootstrapURLKeys={{ key: "" }}
                                defaultCenter={defaultProps.center}
                                defaultZoom={defaultProps.zoom}
                                yesIWantToUseGoogleMapApiInternals
                                onGoogleApiLoaded={({ map, maps }) => handleApiLoaded(map, maps)}
                            />
                                {/* {
                                    eventArr.map((event, index)=>(
                                        <AnyReactComponent
                                            lat={event.lat}
                                            lng={event.lng}
                                            open={true}
                                            onClose={handleTooltipClose}
                                            text="My Marker"
                                            onClick={handleTooltipOpen}
                                        />
                                    ))
                                }
                                 */}

                        </div>

                    </div>
                </div>

            </div>
        </Main>
    )
}
