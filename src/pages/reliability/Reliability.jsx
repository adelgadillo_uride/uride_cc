import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Card from "@material-ui/core/Card";
import "./reliability.css";
import { Button, ButtonGroup, CardActions, CardContent, IconButton } from '@material-ui/core';
import { Apps, List, CalendarToday, ArrowUpward, ArrowDropDown, ArrowDownward } from "@material-ui/icons";
import axios from 'axios'

import CityReport from './CityReport';
import Main from '../../components/main/Main';
import Skeleton from '@material-ui/lab/Skeleton';
import { Dialog } from '@material-ui/core';
import { Calendar } from '../../components/dateRangePicker/Calendar';


const useTabStyles = makeStyles({
  root: {
    justifyContent: "center",
    flexGrow: 1,
  },
  paper: {
    textAlign: 'center',
    border: 'none'
  },
  scroller: {
    flexGrow: "0"
  },
  tabContainer: {
    width: '100%',
    flexGrow: "1"
  }
});


export default function Reliability(props) {
  const classes = useTabStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const [selectedDate, setSelectedDate] = useState("Today");

  const user = JSON.parse(localStorage.getItem('user'));

  /**
   * The state for the calendar
   */
  const [state, setState] = useState([
    {
      startDate: new Date(),
      endDate: new Date(),
      key: 'selection',
      // label: selectedDate
    }
  ]);

  const [open, setOpen] = useState(false);

  const handleSelection = (item) => {
    setState([item.selection]);
    setSelectedDate(item.selection.label || "Custom");
    getData();
    setOpen(false);
  }
  
  const [cities, setCities] = useState([]);

  // const cities = [
  //   "Belleville",
  //   "Chatham-Kent",
  //   "North Bay",
  //   "Peterborough",
  //   "Sault Ste Marie",
  //   "Sudbury",
  //   "Thunder Bay",
  //   "Timmins"
  // ];

  const getData = () => {
    const [selection] = state;
    const startDate = selection.startDate.toLocaleDateString("en-CA");
    const endDate = selection.endDate.toLocaleDateString("en-CA");
    // axios.get('https://uri-dev-cc-kpis-api-tbyptzvcgq-uk.a.run.app/v1/reliability/growth-by-city'
    axios.get(`https://uri-dev-cc-reliability-api-tbyptzvcgq-uk.a.run.app/v1/cities?format=hours&from=${startDate}&to=${endDate}`
      , {
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': user.token
        }
      }
    )
      .then(response => {
        const { data } = response;
        setCities([...data.items]);

        // return response.json();
      })
      .catch(err => console.log(err))
    // .then(function (myJson) {
    //     setDrivers(myJson);
    // });
  }
  useEffect(() => {
    props.setAppbarTitle(props.title);
    getData()
  }, []);

  return (
    <Main className="mainContent">

      <div className={classes.root}>
        <Box>
          <AppBar position="static" color="default">
            {cities.length < 1 ? <Skeleton /> : (
              <Tabs
                value={value}
                onChange={handleChange}
                indicatorColor="primary"
                textColor="primary"
                variant="scrollable"
                scrollButtons="auto"
                aria-label="scrollable auto tabs example"
              >

                <Tab label="Summary" {...a11yProps(0)} />
                {
                  cities.map((element, index) =>

                    <Tab key={element.city} label={element.city} {...a11yProps(index + 1)} />

                  )
                }
              </Tabs>
            )
            }
          </AppBar>

        </Box>
        <br />

        <TabPanel value={value} index={0}>
          <div className="header">
            <h3>Reliability Summary</h3>
            <div className="optionButtons">
              <ButtonGroup color="primary" aria-label="outlined primary button group">
                <IconButton >
                  <List />
                </IconButton>
                <IconButton >
                  <Apps />
                </IconButton>

              </ButtonGroup>

              <Button
                variant="contained"
                color="default"
                className={classes.button}
                startIcon={<CalendarToday />}
                endIcon={<ArrowDropDown />}
                onClick={()=>setOpen(true)}
              >
                {selectedDate}
              </Button>
            </div>

          </div>
          <br />
          <Grid container spacing={1}>
            {cities.length < 1 ? <Skeleton /> : (
              <>
                {

                  cities.map((city) =>
                    <Grid key={city.city} item xs={12} sm={6} lg={3}>
                      <Card>
                        <CardContent>
                          <span className="cardTitle">
                            {city.city}
                          </span>
                          <p className="cardAcceptanceRate">
                            {city.rate}%
                          </p>
                          <div className="cardDetails">
                            <span>Today:</span>
                            <span className="featuredMoneyRate">
                              {city.pastRate < city.rate && <><ArrowUpward className="featuredIcon" />{city.growth}%</>}
                              {city.pastRate > city.rate && <><ArrowDownward className="featuredIcon" style={{ color: 'red' }} />{city.growth}%</>}
                              {city.pastRate === city.rate && <>{city.growth}%</>}
                            </span>
                          </div>
                          <div className="cardDetails">
                            <span>Yesterday:</span>
                            <span className="featuredMoneyRate">{city.pastRate}%</span>
                          </div>
                          <div className="cardDetails pd-v16">

                            <div className="">
                              <span className="indicators">Trips</span>
                              <h3>{city.successTrips}</h3>
                            </div>
                            <div className="">
                              <span className="indicators">Schedule</span>
                              <h3>{city.scheduled}</h3>
                            </div>
                            <div className="">
                              <span className="indicators">Online</span>
                              <h3>{city.online}</h3>
                            </div>
                            <div className="">
                              <span className="indicators">Offline</span>
                              <h3>{city.offline}</h3>
                            </div>
                          </div>
                        </CardContent>
                        <CardActions>
                          <Button size="medium" color="primary">
                            Download
                          </Button>
                          <Button size="medium" color="primary">
                            Email
                          </Button>
                        </CardActions>
                      </Card>
                    </Grid>
                  )
                }
              </>
            )}


          </Grid>
        </TabPanel>
        {
          cities.map((element, index) =>
            <TabPanel value={value} index={index + 1}>
              <CityReport city={element} state={state} selectedDate={selectedDate} />
            </TabPanel>
          )
        }
      </div>
      <Dialog open={open} onClose={()=>setOpen(false)}>
          <Calendar
              handleSelection={handleSelection}
              state={state}
              selectedDate={selectedDate}
          />;
      </Dialog>
    </Main>

  );
};


function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    'aria-controls': `scrollable-auto-tabpanel-${index}`,
  };
}