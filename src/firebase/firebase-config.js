import firebase from "firebase/compat/app";
import 'firebase/compat/firestore';
import 'firebase/compat/auth';

// const firebaseConfig = {
//     apiKey: process.env.REACT_APP_APIKEY,
//     authDomain: process.env.REACT_APP_AUTHDOMAIN,
//     databaseURL: process.env.REACT_APP_DATABASEURL,
//     projectId: process.env.REACT_APP_PROJECTID,
//     storageBucket: process.env.REACT_APP_STORAGEBUCKET,
//     messagingSenderId: process.env.REACT_APP_MESSAGINGSENDERID,
//     appId: process.env.REACT_APP_APPID,
//     measurementId: process.env.REACT_APP_MEASUREMENTID
// };

const firebaseConfig = {
  apiKey: "AIzaSyCYLqk2yR1EW2Vl03Egf-4ZW7VaPKC4NU0",
  authDomain: "uride-269306.firebaseapp.com",
  databaseURL: "https://uride-269306.firebaseio.com",
  projectId: "uride-269306",
  storageBucket: "uride-269306.appspot.com",
  messagingSenderId: "726817814091",
  appId: "1:726817814091:web:69b515c94601eae2627fce",
  measurementId: "G-MP18EMEN8S"
};

// if (process.env.NODE_ENV === 'test') {
//   // Initialize Firebase
//   firebase.initializeApp(firebaseConfigTest);
// } else {
//   // Initialize Firebase
//   firebase.initializeApp(firebaseConfig);
// }

// console.log(process.env);

firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
const fb = firebase;
export {
    db, googleAuthProvider, fb
};