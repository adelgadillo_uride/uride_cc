import { actionTypes } from "../actionTypes/actionTypes"

export const setError = (err) => ({
    type: actionTypes.uiSetError,
    payload: err
});

export const removeError = () => ({
    type: actionTypes.uiRemoveError
});

export const startLoading = () =>({
    type: actionTypes.uiStartLoading,
    payload: true
});

export const finishLoading = () =>({
    type: actionTypes.uiFinishLoading,
    payload: false
});