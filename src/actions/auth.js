import { actionTypes } from "../actionTypes/actionTypes"
import { fb, googleAuthProvider } from "../firebase/firebase-config";
import { finishLoading, startLoading } from "./ui";
import Swal from "sweetalert2";
import axios from "axios";

export const authAPI = 'https://uri-dev-cc-passport-api-tbyptzvcgq-uk.a.run.app/v1';

export const startLoginEmailPassword = (email, password) => {
    return (dispatch) => {
        /**
         * funcion dispatch viene del middleware thunk
         */

        dispatch(startLoading());
        return fb.auth().signInWithEmailAndPassword(email, password)
        .then(({user}) => {
            dispatch(finishLoading());
            dispatch(login(user.uid, user.displayName, user.photoURL, user.email));
        })
        .catch(err=>{
            dispatch(finishLoading());
            Swal.fire('Error', err.message, 
            'error');
            // console.log(err);
        });
    }
}

export const startRegisterEmailPassword = (email, password, name) => {
    return (dispatch) => {
        fb.auth().createUserWithEmailAndPassword(email, password)
        .then(async ({user}) => {
            await user.updateProfile({
                displayName: name
            });
            dispatch(login(user.uid, user.displayName, user.photoURL, user.email));
        })
        .catch(err=>{
            Swal.fire('Error', err.message, 
            'error');
            console.log(err);
        });

        // setTimeout(() => {
        //     dispatch(login(123456, 'Carlitox'))
        // }, 2500);
    }
}


export const startGoogleLogin = () => {
    return (dispatch)=>{
        fb.auth().signInWithPopup(googleAuthProvider)
            .then(({user, credential}) => {
                // localStorage.setItem('user', JSON.stringify(user));
                debugger;
                // dispatch(saveCredential(credential));

                const {idToken} = credential;

                doLogin(user, idToken).then(result=> {
                    const {data} = result;
                    const loginUser = {
                        uid: user.uid,
                        displayName: user.displayName,
                        photoURL: user.photoURL,
                        email: user.email,
                        token: data.token,
                        refreshToken: data.refreshToken

                    };
                    localStorage.setItem('user', JSON.stringify(loginUser));
                    dispatch(login(user.uid, user.displayName, user.photoURL, user.email, data.token, data.refreshToken));
                    Swal.fire('Welcome', `You have logged in as ${user.displayName}`,'success');
                }).catch(err=> {
                    Swal.fire('Error', err.message, 
                    'error');
                    console.log(err);
                });
                
            }).catch(err=>{
                Swal.fire('Error', err.message, 
                'error');
                console.log(err);
            });
    }
}

export const doLogin = (user, accessToken) => {
    const userPost = {
        type:'GOOGLE',
        fullName: user.displayName,
        firstName: user.displayName.split(" ")[0],
        lastName: user.displayName.split(" ")[1],
        email: user.email,
        token: accessToken
    };
    return axios.post(`${authAPI}/login`, userPost);
}

export const login = (uid, displayName, photoURL, email, token, refreshToken) => ({
    type: actionTypes.login,
    payload: {
        uid,
        displayName,
        photoURL,
        email,
        token,
        refreshToken
    }
});

export const startLogout = () => {
    return async (dispatch) => {
        fb.auth().signOut();
        localStorage.removeItem('user');
        dispatch(logout());
        // dispatch(logoutClear());
    }
}

export const logout = () => ({
    type: actionTypes.logout
});

export const saveCredential = (credential) => ({
    type: actionTypes.saveCredential,
    payload: {
        credential
    }
});

export const auth = (data) => ({
    type: actionTypes.auth,
    payload: {
        token: data.token,
        refreshToken: data.refreshToken
    }
})